<?php
namespace Frame\Module;

// Controllers
use Frame\Module\Admin\Controller\OverviewController;
use Frame\Module\Admin\Controller\UsersController;
use Frame\Module\Admin\Controller\RoutesController;

// Middlewares
use Frame\Module\Admin\Middleware\AdminMiddleware;
use Frame\Module\Admin\Middleware\Stats\StatsPagesMiddleware;
use Frame\Module\Admin\Middleware\Stats\StatsVisitorsMiddleware;

use Frame\Module\Admin\Model\User;

class Admin extends \Frame\Module
{
	const MIGRATIONS = __DIR__ . '/Admin/Database/Migrations';
	const VIEWS = __DIR__ . '/Admin/Ressources/views';
	const LANGUAGES = __DIR__ . '/Admin/Lang';

    public function __invoke(){
    	$that = $this;
    	$c = $this->app->getContainer();

		$c['admin'] = function(){ return new User(); };

		$c->view->getEnvironment()->addFunction(new \Twig_SimpleFunction('is_role', function ($role) use($c) {
			return $c['admin']->isRole($role);
		}));

		$this->app->group('/admin', function() use ($c, $that){
			$this->get('', function($req, $res, $args) use ($c, $that){
	    		return $res->withRedirect($this->router->pathFor($that->prefix . '.overview'));
	    	})->setName($that->prefix . '.home');

			$this->map(['GET', 'POST'], '/overview', OverviewController::class)->setName($that->prefix . '.overview');
			$this->map(['GET', 'POST'], '/users', UsersController::class)->setName($that->prefix . '.users');
			$this->map(['GET', 'POST'], '/routes', RoutesController::class)->setName($that->prefix . '.routes');
		})->add(AdminMiddleware::class);

		$this->app->add(StatsPagesMiddleware::class);

		// View nav update
        /*$c->ViewData->merge([
			[
				'file' => '@Admin/parts/nav/admin-item.twig'
			],
        ], ['navbar', 'right'], true);*/
    }
}