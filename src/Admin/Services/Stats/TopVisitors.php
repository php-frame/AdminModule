<?php 
namespace Frame\Module\Admin\Services\Stats;

use Frame\Module\Admin\Model\Stats\StatsPage;

class TopVisitors extends StatModule{
	protected $nbItems = 5;

	public function getArgs(){
		
		$rows = StatsPage::where('created_at', '>', new \Carbon\Carbon('first day of this month'))
			->limit($this->nbItems)
			->selectRaw('ip, browser, operating_system, count(*) as \'hits\'')
            ->groupBy('ip', 'browser', 'operating_system')
            ->orderBy('hits', 'DESC')
            ->get();

		return [
			'top_items' => $rows,
			'nb_items' => $this->nbItems
		];
	}
}