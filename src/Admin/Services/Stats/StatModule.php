<?php 
namespace Frame\Module\Admin\Services\Stats;

class StatModule{

	public function getFile(){
		return '@Admin/stats/' . strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', (new \ReflectionClass($this))->getShortName())) . '.twig';
	}

	public function getArgs(){
		return [];
	}
}