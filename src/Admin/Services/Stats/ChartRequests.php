<?php 
namespace Frame\Module\Admin\Services\Stats;

use Carbon\Carbon;
use Frame\Module\Admin\Model\Stats\StatsPage;

class ChartRequests extends StatModule{
	public function getArgs(){

		$dailyRequests = StatsPage::where('created_at', '>', Carbon::now()->subMonth())
			->selectRaw('DATE(created_at) as x, count(*) as y')
			->groupBy('x')
			->get()
			->toArray();

		$labels = [];
		$startDate = Carbon::now()->subMonth();
		while($startDate <= Carbon::now()){
			$labels[] = $startDate->toDateString();
			$startDate->addDay();
		}

		return [
			'chart_data' => [
				'type' => 'line',
				'data' => [
					'labels' => $labels,
					'datasets' => [ 
						[ 
							'backgroundColor' => 'rgba(52, 152, 219, 0.3)',
							'borderColor' => '#3498db',
							'label' => 'Requests',
							'data' => $dailyRequests 
						]
					]
				],
				'options' => [ 
					'responsive' => true, 
					'maintainAspectRatio' => false, 
					
					'scales' => [
						'xAxes' => [[
			              'type' => 'time',
			              'time' => [
			                'unit' => 'day',
			                'min' => Carbon::now()->subMonth()->toDateString(),
			                'max' => Carbon::now()->toDateString()
			              ]]
			            ],
			            'yAxes' => [
			            	[
				                'ticks' => [
				                    'beginAtZero' => true,

				                ]
			                ]
			            ]
			        ]
				]
			]
		];
	}
}