<?php 
namespace Frame\Module\Admin\Services;

use Frame\Module\Admin\Services\Stats\TopPages;
use Frame\Module\Admin\Services\Stats\TopVisitors;
use Frame\Module\Admin\Services\Stats\ChartRequests;

class StatsService{
	protected $modules = [
		ChartRequests::class,
		TopPages::class,
		TopVisitors::class
	];

	public function __invoke(){
		$renderData = [];
		foreach($this->modules as $module){
			$renderData[] = new $module;
		}
		return $renderData;
	}
}