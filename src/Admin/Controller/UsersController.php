<?php
namespace Frame\Module\Admin\Controller;

use Frame\Module\Admin\Controller\AdminController;

use Frame\Module\Admin\Model\User;

class UsersController extends AdminController
{
    public function get()
    {
    	$this->viewData['users'] = User::all();
        return $this->render('@Admin/pages/users');
    }
}
