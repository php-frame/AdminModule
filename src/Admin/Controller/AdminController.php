<?php
namespace Frame\Module\Admin\Controller;

use Frame\Controller\Controller;

class AdminController extends Controller
{
    public function __construct($container)
    {
    	parent::__construct($container);

    	$this->ViewData->merge(['modules' => $this->Modules]);
    }
}
