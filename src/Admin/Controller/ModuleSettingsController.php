<?php
namespace Frame\Module\Admin\Controller;

use Frame\Controller\Controller;

class ModuleSettingsController extends AdminController
{

	protected $file;
	protected $args = [];

    public function get(){
    	return $this->render('@Admin/layout/module_settings_layout', [ 'content' => $this->view->fetch($this->getFile(), $this->getArgs()) ]);
    }

    public function getFile(){
    	return $this->file;
    }

    public function getArgs(){
    	return $this->args;
    }
}
