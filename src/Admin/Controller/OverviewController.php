<?php
namespace Frame\Module\Admin\Controller;

use Frame\Module\Admin\Controller\AdminController;
use Frame\Module\Admin\Services\StatsService;

class OverviewController extends AdminController
{
    public function get()
    {
        return $this->render('@Admin/pages/overview', [ 'stats' => (new StatsService)() ]);
    }
}
