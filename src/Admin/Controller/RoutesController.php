<?php
namespace Frame\Module\Admin\Controller;

use Frame\Module\Admin\Controller\AdminController;

use Frame\Module\Admin\Model\User;

class RoutesController extends AdminController
{
    public function get()
    {
    	$this->viewData['routes'] = $this->router->getRoutes();
        return $this->render('@Admin/pages/routes');
    }
}
