<?php
namespace Frame\Module\Admin\Middleware;

use Frame\Middleware\Middleware;

class AdminMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        if(!$this->container->admin->isRole('admin')) {
            $this->flash('warning', $this->lang('alerts.requires_role', [ 'role' => 'admin' ]));
            return $this->back($request, $response);
        }
        
        $response = $next($request, $response);
        return $response;
    }
}
