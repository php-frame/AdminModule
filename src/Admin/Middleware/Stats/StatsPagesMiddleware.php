<?php
namespace Frame\Module\Admin\Middleware\Stats;

use Frame\Middleware\Middleware;
use Frame\Module\Admin\Model\Stats\StatsPage;

use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;

class StatsPagesMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        $response = $next($request, $response);
        $route = $request->getAttribute('route');

        /*$adapter  = new \Http\Adapter\Guzzle6\Client();
        $provider = new \Geocoder\Provider\FreeGeoIp\FreeGeoIp($adapter);
        $geocoder = new \Geocoder\StatefulGeocoder($provider, 'en');

        $infos = $geocoder->geocode($request->getAttribute('ip_address'));*/


        StatsPage::create([
        	'name' => $route ? $route->getName() : '-',
        	'path' => $request->getUri()->getPath(),
            'ip' => $request->getAttribute('ip_address'),
            'operating_system' => $request->getAttribute('operating_system'),
            'browser' => $request->getAttribute('browser')
        ]);

        return $response;
    }
}
