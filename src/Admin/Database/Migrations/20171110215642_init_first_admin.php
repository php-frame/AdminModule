<?php


use Phinx\Migration\AbstractMigration;

class InitFirstAdmin extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->insert('roles', [
            'id' => 1,
            'label' => 'admin',
            'rights' => 10
        ]);

        $this->insert('user_roles', [
            'user_id' => 1,
            'role_id' => 1
        ]);
    }
}
