<?php
namespace Frame\Module\Admin\Model\Stats;

use Illuminate\Database\Eloquent\Model;

class StatsPage extends Model{
	protected $fillable = [ 'name', 'path', 'ip', 'browser', 'operating_system' ];
}