<?php 

namespace Frame\Module\Admin\Model;

use Frame\Util\Session;
use Frame\Module\Auth\Guard;

class User extends Guard
{
    public static $ADMIN_CODE = 1;

    public function roles(){
    	return $this->hasMany(Role::class);
    }

    public function isRole($label)
    {
        if(!$this->check()){
            return false;
        }

        return $this->user()->roles()->where('label', $label) != null;
    }
}