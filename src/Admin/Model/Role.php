<?php
namespace Frame\Module\Admin\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [ 'label', 'rights' ];
}