import Chart from 'chart.js'

let charts = document.querySelectorAll('[data-auto-chart]')

charts.forEach(function(node){
	let cv = document.createElement('canvas')
	node.appendChild(cv)

	let chart = new Chart(cv.getContext('2d'), JSON.parse(node.getAttribute('data-auto-chart')))
})