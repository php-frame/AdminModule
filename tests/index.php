<?php 
require __DIR__ . '/../vendor/autoload.php';
session_start();

$app = new \Frame\Core([
	\Frame\Module\Auth::class
]);

$app->run();